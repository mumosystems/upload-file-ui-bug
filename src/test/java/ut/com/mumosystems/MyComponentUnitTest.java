package ut.com.mumosystems;

import org.junit.Test;
import com.mumosystems.api.MyPluginComponent;
import com.mumosystems.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}