package com.mumosystems.action;

import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import java.util.HashMap;
import java.util.Map;

public class FormContext extends AbstractJiraContextProvider {
    @Override
    public Map getContextMap(ApplicationUser applicationUser, JiraHelper jiraHelper) {
        Map<String, Object> context = new HashMap();
        context.put("projectKey",jiraHelper.getRequest().getParameter("projectKey"));
        System.out.println(jiraHelper.getRequest().getParameter("projectKey"));
        return context;
    }
}
