package com.mumosystems.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UploadFormRenderAction extends JiraWebActionSupport {
    public String doDefault() throws Exception {
        log.debug("Entering doExecute");
        HttpServletRequest request = getHttpRequest();
        HttpServletResponse response = getHttpResponse();
        request.getParameter("projectKey");

        return SUCCESS;
    }
}
